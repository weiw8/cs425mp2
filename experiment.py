import os
import time
import sys


# Open clients, on ports 9000+n
num_clients = int(sys.argv[1])
total_client = sys.argv[5]
vm_number = sys.argv[4]
serviceaddr = sys.argv[2]
serviceport = sys.argv[3]
for i in range(num_clients):
    newpid = os.fork()
    if newpid == 0:
        nodename = vm_number+"_"+str(i)
        print(nodename+ ": "+str(os.getpid()))
        port = 9000 + i
        os.system("java ConnectionPort " + serviceaddr+" "+serviceport+" "+ nodename+ " " + str(port) +" "+total_client)
        break
    else:
        time.sleep(0.1)
