import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Block {
    private int height;
    private String[] transactionlist;
    private String previousBlockHash;
    private String sourceAddr;
    private String puzzle;
    private String solution;
    private String blockHash;

    private String calculateHash(boolean type)
    throws NoSuchAlgorithmException
    {//true:blockhash,false:
        StringBuilder sbuf = new StringBuilder();
        sbuf.append(previousBlockHash);
        for (String tx:transactionlist) {
            sbuf.append(tx);
        }
        if (type)
            sbuf.append(solution);
            MessageDigest md = MessageDigest.getInstance("SHA-256");

        // digest() method called
        // to calculate message digest of an input
        // and return array of byte
        byte[] intermidateResult = md.digest(sbuf.toString().getBytes(StandardCharsets.UTF_8));
        BigInteger number = new BigInteger(1, intermidateResult);

        // Convert message digest into hex value
        StringBuilder hexString = new StringBuilder(number.toString(16));

        // Pad with leading zeros
        while (hexString.length() < 32)
        {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }

    public Block(int height, String previousBlockHash, String[] transactionlist, String sourceAddr) {
        this.transactionlist = transactionlist;
        this.height = height;
        this.previousBlockHash = previousBlockHash;
        this.sourceAddr = sourceAddr;
    }
    public Block(int height, String previousBlockHash, String[] transactionlist, String sourceAddr, String puzzle, String solution, String blockHash) {
        this.height = height;
        this.previousBlockHash = previousBlockHash;
        this.transactionlist = transactionlist;
        this.sourceAddr = sourceAddr;
        this.puzzle = puzzle;
        this.solution = solution;
        this.blockHash = blockHash;
    }
    public String getSourceAddr() {
        return sourceAddr;
    }
    public int getHeight() {
        return height;
    }
    public String[] getTransactionList() {
        return transactionlist;
    }
    public String getPreviousBlockHash() {
        return previousBlockHash;
    }
    public String getHash()
    throws NoSuchAlgorithmException
    {
       if (blockHash == null)
           blockHash = calculateHash(true);
       return blockHash;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String s)
    throws NoSuchAlgorithmException
    {
        solution = s;
        blockHash = calculateHash(true);
    }

    public String getPuzzle()
    throws NoSuchAlgorithmException
    {
        if (puzzle == null)
            puzzle = calculateHash(false);
        return puzzle;
    }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(height + ";" + previousBlockHash+";" + transactionlist[0]);
        for (int i = 1; i < transactionlist.length;i++) {
            s.append(" ");
            s.append(transactionlist[i]);
        }
        s.append(";");
        s.append(sourceAddr);
        s.append(";");
        try {
        String mypuzzle = getPuzzle();
        String mysolution = getSolution();
        String myhash = getHash();
        s.append(mypuzzle);
        s.append(";");
        s.append(mysolution);
        s.append(";");
        s.append(myhash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return s.toString();
    }
}