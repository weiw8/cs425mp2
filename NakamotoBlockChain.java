import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.ReentrantLock;


import java.util.concurrent.ConcurrentHashMap;
import java.security.NoSuchAlgorithmException;

 
public class NakamotoBlockChain {

    private final int transactionLimit = 300;
    private final int selfmineinterval = 500;
    //private final int listeninterval; not sure whether need it yet
    private Stack<Block> blockChain;
    private ConcurrentHashMap<String,Integer> memPool;
    private HashSet<String> tempTxs;//has been commited before because of blocks received from other. 
    private HashMap<Integer,ArrayList<String>> tempTxsdict;//which block is this temporarily commited transaction belongs to
    private int currentHeight;
    private final ReentrantLock token;
    private HashMap<String,Integer> account;
    String myaddr;
    ConnectionPort cp;

    private PrintStream transactionappearLog;
    private PrintStream splitLog;
    //function to validate transaction and update account
    private boolean validateTx(String tx, HashMap<String,Integer> account) {
        String trans[] = tx.split(",");
        String src = trans[2];
        String dest = trans[3];
        int amt = Integer.parseInt(trans[4]);
        if (src.equals("0")) {
            account.put(dest,account.getOrDefault(dest,0) + amt);
            return true;
        }
        int srcAccBal = account.getOrDefault(src,0);
        int destAccBal = account.getOrDefault(dest,0);
        int updatedBal = srcAccBal - amt;
        //Only update if resulting balance is positive
        if(updatedBal >=0){
            account.put(src,updatedBal);
            account.put(dest, destAccBal+amt);
            return true;
        } else {
            return false;
        }

    }

    //thread to rollback transaction
    class RollBackTx implements Runnable {
        Block b;
        public RollBackTx(Block b) {
            this.b = b;
        }
        public void run() {
            ArrayList<String> prev = tempTxsdict.remove(b.getHeight());
            if (prev != null) {
            tempTxs.removeAll(prev);
            }
            String[] txs = b.getTransactionList();
            for (int j = txs.length - 1; j >=0 ; j--) {
                rollbackTxAccount(txs[j]);
                memPool.put(txs[j],1);
            }
        }

        private void rollbackTxAccount(String tx) {
            String trans[] = tx.split(",");
            String src = trans[2];
            String dest = trans[3];
            int amt = Integer.parseInt(trans[4]);
            if (!src.equals("0")) {
                account.put(src,account.get(src) + amt);
            }
            account.put(dest,account.get(dest) - amt);
        }
    }
 
    //function to merge forks
    private void mergeFork(Block block) {
        Stack<Block> newblocks = new Stack<>();
        try {
            int splitlength = 0;
        while (!blockChain.isEmpty() && !block.getPreviousBlockHash().equals(blockChain.peek().getHash())) {
            splitlength ++;
            Block oldblock = blockChain.pop();
            Thread torollback = new Thread(new RollBackTx(oldblock));
            torollback.start();
            block = cp.requestBlock(block.getSourceAddr(), block.getPreviousBlockHash());
            newblocks.push(block);
            try {
            torollback.join();
            }catch (InterruptedException e) {
                e.printStackTrace();
            } 
        }
        //log split length
        splitLog.println(splitlength);
        while (!newblocks.isEmpty()) {
            Block b = newblocks.pop();
            blockChain.push(b);
            for (String tx:b.getTransactionList()) {
                if (memPool.remove(tx)==null) {
                    tempTxs.add(tx);
                    tempTxsdict.computeIfAbsent(b.getHeight(),a -> new ArrayList<String>()).add(tx);
                }
                validateTx(tx,account);
            }
        }
        }catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    //thread to mine block
    class SelfMineBlock implements Runnable {
        public void run() {
            while (true) {
                try {
                Thread.sleep(selfmineinterval);
                }catch(InterruptedException e) {
                    e.printStackTrace();
                }
                HashMap<String,Integer> tentativeaccount = null;
                String prevBlockHash = "";
                int blockheight = 0;
                String[] tentative = null;
                token.lock();
                try {
                tentativeaccount = new HashMap<>(account);
                if(blockChain.size() > 0){
                    prevBlockHash = blockChain.peek().getHash();
                }
                blockheight = currentHeight + 1;
                
                    //lock to control access to object related to blockchain,account
                    if (memPool.size() < transactionLimit) {
                        
                        continue;
                    }
                tentative = memPool.keySet().toArray(new String[memPool.size()]);
                }catch(NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                finally {
                    token.unlock();
                }
                Arrays.sort(tentative,new EventComparator());//sort the transactions by generation time
                ArrayList<String> newTransactionsList = new ArrayList<>();
                int i=0;
                for(String tx:tentative){
                    //this transaction has been commited before, should be removed from memPool
                    if (tempTxs.contains(tx)) {
                        memPool.remove(tx);
                        continue;
                    }
                    if (validateTx(tx, tentativeaccount)) {
                        //only add valid transactions and transactions that has not been commited before
                        newTransactionsList.add(tx);
                        i++;
                    }
                    if(i==2000){
                        break;
                    }
                }
                token.lock();
                try {
                    if (currentHeight + 1 != blockheight) {
                        
                        continue;
                    }
                }
                finally {
                    token.unlock();
                }
                String[] txns = newTransactionsList.toArray(new String[i]);
                Block tentativeblock = new Block(blockheight,prevBlockHash,txns, myaddr);
                try{
                String puzzle = "SOLVE "+tentativeblock.getPuzzle();
                cp.sendPuzzle(puzzle);
                }catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                String solution = cp.getSolution();
                token.lock();
                try {
                    if(tentativeblock.getHeight() == currentHeight + 1) {
                        currentHeight ++;
                    //update blockchain
                    tentativeblock.setSolution(solution.split(" ")[2]);
                    cp.sendBlock(tentativeblock);
                    blockChain.push(tentativeblock);
                    account = tentativeaccount;
                    for (String tx:tentativeblock.getTransactionList()) {
                        //this tx appears in a block
                        transactionappearLog.println(cp.getMicroTime(tx.split(",")[0]) + ","+cp.getMicroTime());
                        memPool.remove(tx);
                    }
                    }
                }catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                finally {
                    token.unlock();
                }   
            }
        }

    }

    //thread receive block from other nodes
    class ListenForBlock implements Runnable {
        public void run() {
            while (true) {
                Block receivedblock = cp.getBlock();
                token.lock();
                try {
                    if (receivedblock.getHeight() >= currentHeight + 1) {
                        String verification = "VERIFY " + receivedblock.getPuzzle() + " "+receivedblock.getSolution();
                        cp.sendVerify(verification);
                    } else {
                        
                        continue;
                    }
                }
                catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                finally {
                    token.unlock();
                }
                String verified = cp.getVerified();
                if (verified.split(" ")[1].equals("OK")) {
                    token.lock();
                    try {
                        //try to update block chain
                        if (receivedblock.getHeight() >= currentHeight + 1) {
                            currentHeight = receivedblock.getHeight();
                            //update block chain
                            Block previousblock = null;
                            if (!blockChain.isEmpty()) {
                                previousblock = blockChain.peek();
                            }
                            
                            int wantedsize = receivedblock.getHeight() - blockChain.size() - 1;
                            Block[] wantedblocks = new Block[wantedsize + 1];
                            wantedblocks[wantedsize] = receivedblock;
        
                            while (wantedsize > 0) {
                                wantedblocks[wantedsize - 1] = cp.requestBlock(wantedblocks[wantedsize].getSourceAddr(), wantedblocks[wantedsize].getPreviousBlockHash());
                                wantedsize --;
                            }
                            if (previousblock!=null && !wantedblocks[0].getPreviousBlockHash().equals(previousblock.getHash())) {
                                        //need switch and merge
                                        mergeFork(wantedblocks[0]);
                            }
                                //don't need switch and merge
                            for (Block b:wantedblocks) {
                                blockChain.push(b);
                                for (String tx:b.getTransactionList()) {
                                    if (memPool.remove(tx)==null) {
                                        tempTxs.add(tx);
                                        tempTxsdict.computeIfAbsent(b.getHeight(),a -> new ArrayList<String>()).add(tx);
                                    }
                                    validateTx(tx,account);
                                }
                            }   
                        }
                    } catch(NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    finally {
                        token.unlock();
                    }
                }
            }
        }

    }

    public NakamotoBlockChain(String serviceaddr, int serviceport, String nodename, int myport, int total) 
    throws IOException
    {
        cp = new ConnectionPort(serviceaddr, serviceport, nodename,myport, total);
        memPool = cp.getMemPool();
        blockChain = new Stack<>();
        account = new HashMap<>();
        tempTxs = new HashSet<>();
        tempTxsdict = new HashMap<>();
        currentHeight = 0;
        token = new ReentrantLock();
        myaddr = cp.getAddress();
        File tplog = new File("tp_"+nodename+".csv");
        tplog.createNewFile();
        transactionappearLog = new PrintStream(new FileOutputStream(tplog));
        File splog = new File("split_"+nodename+".csv");
        splog.createNewFile();
        splitLog = new PrintStream(new FileOutputStream(splog));
        new Thread(new SelfMineBlock()).start();
        new Thread(new ListenForBlock()).start();
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 5) {
            System.out.println("usage: NakamotoBlockChain serveraddr serverport myname myport initialnode");
            return;
        }
        NakamotoBlockChain testtx = new NakamotoBlockChain(args[0],Integer.parseInt(args[1]),args[2],Integer.parseInt(args[3]),Integer.parseInt(args[4]));
    }
   
}