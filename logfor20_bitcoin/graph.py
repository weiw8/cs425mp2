# -*- coding: utf-8 -*-
"""
Created on Sat Feb  8 00:08:38 2020

@author: kagura
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv

vm = [2,3]
nodenum = {2:10,3:10}
#delay graph
delaylogs = []

for j in vm:
    for i in range(nodenum[j]):
        nodename = str(j)+"_"+str(i)
        fname = "delay_"+nodename+".csv"
        delaylog = pd.DataFrame([])
        with open(fname,'r') as csvfile:
            reader = csv.reader(csvfile)
            delivertime=[]
            generatetime = []
            for row in reader:
                delivertime.append(row[0])
                generatetime.append(row[1])
            delaylog[nodename]=delivertime
            delaylog["id"]=generatetime
        delaylogs.append(delaylog)

finalframe = pd.merge(delaylogs[0],delaylogs[1],how='outer')
for i in range(2,len(delaylogs)):
    finalframe = finalframe.merge(delaylogs[i],how='outer')
propagate = pd.DataFrame([])
for j in vm:
    for i in range(nodenum[j]):
        nodename = str(j)+"_"+str(i)
        propagate[nodename]=finalframe[nodename].astype('float')-finalframe["id"].astype('float')
result = pd.DataFrame([])
result['smallest']=propagate.min(axis=1)
result['largest']=propagate.max(axis=1)
result['median']=propagate.median(axis=1)
result['reached']=propagate.shape[1]-propagate.isnull().sum(axis=1)
result['id']=finalframe['id']
result = result.sort_values(by="id")
result['tid']=range(len(result))
plt.figure()
l1,=plt.plot(result['tid'],result['smallest']/1000.0,linewidth=0.5)
l2,=plt.plot(result['tid'],result['largest']/1000.0,linewidth=0.5)
l3,=plt.plot(result['tid'],result['median']/1000.0,linewidth=0.5)
plt.xlabel('transactionId')
plt.ylabel('delay(ms)')
#plt.ylim(0,5000)
plt.title('Smallest,Largest,Median Delay for Each Transaction(20 nodes)')
plt.legend(handles=[l1,l2,l3],labels=['smallest','largest','median'],loc='best')
plt.savefig("20nodetest.png",dpi = 400)
plt.figure()
l1,=plt.plot(result['tid'],result['reached'],linewidth=0.5)
plt.xlabel('transactionId')
plt.ylabel('reached Node')
plt.title('number of nodes each transaction reached(20 nodes)')
plt.savefig("20nodereach.png",dpi=400)

#bandwidth analysis
bandwidthlogs = []
for j in vm:
    for i in range(nodenum[j]):
        nodename = str(j)+"_"+str(i)
        fname = "bandwidth_"+nodename+".csv"
        log = pd.read_csv(fname,names=["time","bytes"])
        bandwidthlogs.append(log)
allbandwidth = pd.concat(bandwidthlogs,ignore_index = True)
allbandwidth = allbandwidth.sort_values(by='time') 
base = allbandwidth['time'][0]
allbandwidth['sec']=np.floor((allbandwidth['time']-base)/1000000)
group = allbandwidth['bytes'].groupby(allbandwidth['sec'])
band = group.sum()
plt.figure()
l1,=plt.plot(band.index,band/1024.0,linewidth=0.5)
plt.xlabel('time(s)')  
plt.ylabel('bandwidth(kb/s)')
plt.title('Bandwidth used by the System(20 nodes)')
plt.savefig("bandwidth20.png",dpi = 400)

#data = pd.read_csv("case1.csv",names=['arrive','length','generate'])
#data.sort_values(by='arrive')

#data['delay']=data['arrive']-data['generate']*1000000000
'''
base = node2exp2deliver['time'][0]
node0exp2deliver['time']=np.floor((node0exp2deliver['time']-base)/1000)
grouped0 = node0exp2deliver['bytes'].groupby(node0exp2deliver['time'])
bandwidth0 = grouped0.sum()
node1exp2deliver['time']=np.floor((node1exp2deliver['time']-base)/1000)
grouped1 = node1exp2deliver['bytes'].groupby(node1exp2deliver['time'])
bandwidth1 = grouped1.sum()
node2exp2deliver['time']=np.floor((node2exp2deliver['time']-base)/1000)
grouped2 = node2exp2deliver['bytes'].groupby(node2exp2deliver['time'])
bandwidth2 = grouped2.sum()
node3exp2deliver['time']=np.floor((node3exp2deliver['time']-base)/1000)
grouped3 = node3exp2deliver['bytes'].groupby(node3exp2deliver['time'])
bandwidth3 = grouped3.sum()
node4exp2deliver['time']=np.floor((node4exp2deliver['time']-base)/1000)
grouped4 = node4exp2deliver['bytes'].groupby(node4exp2deliver['time'])
bandwidth4 = grouped4.sum()
node5exp2deliver['time']=np.floor((node5exp2deliver['time']-base)/1000)
grouped5 = node5exp2deliver['bytes'].groupby(node5exp2deliver['time'])
bandwidth5 = grouped5.sum()
node6exp2deliver['time']=np.floor((node6exp2deliver['time']-base)/1000)
grouped6 = node6exp2deliver['bytes'].groupby(node6exp2deliver['time'])
bandwidth6 = grouped6.sum()
node7exp2deliver['time']=np.floor((node7exp2deliver['time']-base)/1000)
grouped7 = node7exp2deliver['bytes'].groupby(node7exp2deliver['time'])
bandwidth7 = grouped7.sum()
'''
#mindelay = grouped.min()/1000
#maxdelay = grouped.max()/1000
#meddelay = grouped.median()/1000
#quant = grouped.quantile(q=0.9)/1000
#group2 = data['length'].groupby(data['time'])
#bandwidth = group2.sum()

'''
plt.figure()
l1,=plt.plot(bandwidth0.index,bandwidth0,linewidth=0.5)
l2,=plt.plot(bandwidth1.index,bandwidth1,linewidth=0.5)
l3,=plt.plot(bandwidth2.index,bandwidth2,linewidth=0.5)
l4,=plt.plot(bandwidth3.index,bandwidth3,linewidth=0.5)
plt.xlim(0,220)
plt.xlabel('time(s)')
plt.ylabel('bandwidth(bytes/s)')
plt.title('BandWidth of Nodes(8 nodes,node 0,1,2 fails after 100s(node0-node3))')
plt.legend(handles=[l1,l2,l3,l4],labels=['node 0','node 1','node 2','node3'],loc='best')
plt.savefig("exp4bandwidth_1.png",dpi = 400)
plt.figure()
l5,=plt.plot(bandwidth4.index,bandwidth4,linewidth=0.5)
l6,=plt.plot(bandwidth5.index,bandwidth5,linewidth=0.5)
l7,=plt.plot(bandwidth6.index,bandwidth6,linewidth=0.5)
l8,=plt.plot(bandwidth7.index,bandwidth7,linewidth=0.5)
plt.xlim(0,220)
plt.xlabel('time(s)')
plt.ylabel('bandwidth(bytes/s)')
plt.title('BandWidth of Nodes(8 nodes,node 0,1,2 fails after 100s(node4-node7))')
plt.legend(handles=[l5,l6,l7,l8],labels=['node 4','node 5','node 6','node7'],loc='best')
plt.savefig("exp4bandwidth_2.png",dpi = 400)
'''
#plt.xticks(np.linspace(0,115,24),rotation=90)
#plt.xlim(0,250000)
#plt.ylim(0,25000)
'''
plt.xlabel('time(s)')
plt.ylabel('bandwidth(bytes/s)')
plt.title('BandWidth of Nodes(3 nodes,node 0 fails after 100s)')
plt.legend(handles=[l1,l2,l3],labels=['node 0','node 1','node 2'],loc='best')
plt.savefig("exp3bandwidth.png",dpi = 400)
'''
#plt.figure()
#plt.plot(bandwidth.index,bandwidth)
#plt.xticks(np.linspace(0,115,24),rotation=90)
#plt.xlim(15,115)
#plt.xlabel('time(s)')
#plt.ylabel('bandwidth(byte)')
#plt.title('bandwidth track(3 nodes)')
#plt.savefig("band.png",dpi = 400)
